SRCDIR = ./src
OBJDIR = ./obj
OUTDIR = ./out
SERVER = $(OUTDIR)/server
CLIENT = $(OUTDIR)/client
ARCH = $(shell arch)

SERVER_SRCS = \
	$(SRCDIR)/tcpclinet.cpp		\
	$(SRCDIR)/tcpsrv.cpp		\
	$(SRCDIR)/common.cpp		\
	$(SRCDIR)/traces.cpp		\
	$(SRCDIR)/server.cpp		\


SERVER_OBJS = $(addprefix ${OBJDIR}/,$(notdir $(SERVER_SRCS:.cpp=.o)))


CLIENT_SRCS = \
	$(SRCDIR)/tcpclinet.cpp		\
	$(SRCDIR)/tcpsrv.cpp		\
	$(SRCDIR)/common.cpp		\
	$(SRCDIR)/traces.cpp		\
	$(SRCDIR)/client.cpp		\


CLIENT_OBJS = $(addprefix ${OBJDIR}/,$(notdir $(CLIENT_SRCS:.cpp=.o)))


INCPATH = \
	-I/usr/local/include	\
	-I/usr/include			\


LIBPATH = \
	-L/usr/local/lib 		\


LIBS = \
	-lm 		\
	-lstdc++	\
	-lpthread	\


CC 			= g++
LDFLAGS 	= $(LIBS) $(LIBPATH)

ifeq ($(ARCH),x86_64)
	CFLAGS =-c -Wall -O0 -std=c++11 $(INCPATH)
else ifeq ($(ARCH),armv7l)
	CFLAGS = -c -Wall -Ofast -std=c++11 -mfpu=neon -march=armv7 -mtune=native
else
	$(error Bad atchitecture)
endif


.PHONY: all
all: makedirs server client

.PHONY: makedirs
makedirs:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(OUTDIR)

.PHONY: server
server: ${SERVER_OBJS}
	${CC} -o ${SERVER} ${SERVER_OBJS} ${LDFLAGS}

.PHONY: client
client: ${CLIENT_OBJS}
	${CC} -o ${CLIENT} ${CLIENT_OBJS} ${LDFLAGS}

${OBJDIR}/%.o: ${SRCDIR}/%.cpp
	${CC} ${CFLAGS} $< -o $@

.PHONY: clean
clean:
	rm -f $(OUTDIR)/*
	rm -f $(OBJDIR)/*
	find -type f -name "*.o" -delete
	find -type f -name "*~" -delete
