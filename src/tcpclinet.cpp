#include <memory>
#include <string>
#include <cstring>
#include <stdexcept>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "tcpclient.h"
#include "common.h"
#include "traces.h"
#include "sockets.h"


using namespace std;


TCPClient::TCPClient(string const& host, int port)
{
    dbg_msg("connecting to the server ", host, ":", port, "..");
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock < 0)
        throw runtime_error("can't open socket");

    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(host.c_str());
    addr.sin_port = htons(port);

    int res = connect(sock, (sockaddr const*)&addr, sizeof(addr));
    if (res < 0)
        throw runtime_error("can't connect to host '" + host + ":" + to_string(port) + "'");

    dbg_msg("connected");
}

TCPClient::~TCPClient()
{
    close(sock);
    dbg_msg("disconnected");
}

bool TCPClient::write(char const* s, int len)
{
    int res = send(sock, s, len, 0);
    return res == len;
}

int TCPClient::read(char* s, int len, bool blocking)
{
    int flags = blocking ? 0 : MSG_DONTWAIT;
    int status = recv(sock, s, len, flags);
    if (status > 0)
    {
        return status;
    }
    else if (status < 0)
    {
        if (!blocking && (errno == EAGAIN || errno == EWOULDBLOCK))
            return 0;
        dbg_msg("an error occurred");
        return -1;
    }
    else // status == 0
    {
        dbg_msg("connection closed");
        return -1;
    }
}

std::tuple<int, std::string> TCPClient::read(bool blocking)
{
    char buf[1024];
    int status = read(buf, sizeof(buf), blocking);
    if (status < 0)
        return make_tuple(-1, "");
    if (status == 0)
        return make_tuple(0, "");
    return make_tuple(1, string(buf, status));
}

bool TCPClient::write(string const& s)
{
    return this->write(s.data(), s.size());
}

bool TCPClient::available()
{
    return is_socket_alive(sock);
}
