#include <stdexcept>
#include <cstdio>
#include <memory>
#include <vector>
#include "traces.h"

using namespace std;

const vector<string>   logger_prints_enable = {"warnings", "errors", "all"};

void init(log_properties_t* p)
{
    bool enable_info = false;
    bool enable_errs = false;
    bool enable_warns = false;
    bool enable_dbgs = false;

    for (string s : logger_prints_enable)
    {
        if (s == "all")
            enable_info = enable_errs = enable_dbgs = enable_warns = true;
        else if (s == "warnings")
            enable_warns = true;
        else if (s == "errors")
            enable_errs = true;
        else if (s == "infos")
            enable_info = true;
        else if (s == "debugs")
            enable_dbgs = true;
        else
            throw runtime_error("incorrect log parameter");
    }

    p->enable_info = enable_info;
    p->enable_errs = enable_errs;
    p->enable_warns = enable_warns;
    p->enable_dbgs = enable_dbgs;
}

log_properties_t* get_log_properties()
{
    static unique_ptr<log_properties_t> parameters;

    if (parameters)
        return parameters.get();

    parameters.reset(new log_properties_t);
    init(parameters.get());

    return parameters.get();
}
