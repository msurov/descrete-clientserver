#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <stdexcept>

#include "common.h"
#include "traces.h"

#include "tcpsrv.h"
#include "sockets.h"


using namespace std;


/*
* Connection
*/

Connection::Connection(int remote_sock) : 
	remote_sock(remote_sock)
{
}

Connection::~Connection()
{
    close(remote_sock);
    dbg_msg("Connection closed");
}

int Connection::write(char const* s, int len)
{
    int res = send(remote_sock, s, len, 0);
    if (res != len)
    {
        if (errno == EAGAIN || errno == EWOULDBLOCK)
            return 0;
        return -1;
    }
    return 1;
}

int Connection::write(string const& s)
{
    return write(s.data(), s.size());
}

int Connection::read(char* s, int len, bool blocking)
{
    int flags = blocking ? 0 : MSG_DONTWAIT;
    int status = recv(remote_sock, s, len, flags);
    if (status > 0)
    {
        return status;
    }
    else if (status < 0)
    {
        if (!blocking && (errno == EAGAIN || errno == EWOULDBLOCK))
            return 0;
        dbg_msg("an error occurred");
        return -1;
    }
    else // status == 0
    {
        dbg_msg("connection closed");
        return -1;
    }
}

tuple<int, string> Connection::read(bool blocking)
{
    char buf[1024];
    int status = read(buf, sizeof(buf), blocking);
    if (status < 0)
        return make_tuple(-1, "");

    if (status == 0)
        return make_tuple(0, "");

    return make_tuple(1, string(buf, status));
}


/*
 * TCPSrv
 */

TCPSrv::TCPSrv(int port) : 
    port(port), srv_sock(-1)
{
    init_server();
}

TCPSrv::~TCPSrv()
{
    if (srv_sock >= 0)
    {
        shutdown(srv_sock, SHUT_RDWR);
        close(srv_sock);
        srv_sock = -1;
    }
}

shared_ptr<Connection> TCPSrv::wait_for_connection()
{
    int res = listen(srv_sock, SOCK_STREAM);
    throw_if(res, runtime_error("listen socket error"));

    sockaddr_in remote_addr;
    socklen_t addr_len = sizeof(remote_addr);

    while (true)
    {
        int remote_sock = accept(srv_sock, (sockaddr*)&remote_addr, &addr_len);

        if (remote_sock < 0 && errno == EAGAIN)
            continue;

        if (remote_sock < 0 && errno == EINVAL)
            return nullptr;

        if (remote_sock < 0)
            throw runtime_error("can't accept Connection");

        shared_ptr<Connection> ptr;
        ptr.reset(new Connection(remote_sock));
        return ptr;
    }
}

void TCPSrv::stop()
{
    if (srv_sock >= 0)
        shutdown(srv_sock, SHUT_RDWR);
}

void TCPSrv::init_server()
{
    srv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    throw_if(srv_sock < 0, runtime_error("can't open socket"));

    sockaddr_in localhost;
    memset(&localhost, 0, sizeof(localhost));
    localhost.sin_port = htons(port);
    localhost.sin_family = AF_INET;
    localhost.sin_addr.s_addr = INADDR_ANY;

    int res = bind(srv_sock, (sockaddr const*)&localhost, sizeof(localhost));
    if (res)
    {
        close(srv_sock);
        srv_sock = -1;
        runtime_error("can't bind socket to localhost");
    }
}
