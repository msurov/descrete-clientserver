#pragma once

#include <sys/ioctl.h>
#include <sys/socket.h>


inline int available_bytes(int sock)
{
    int bytes_available;
    int status = ioctl(sock, FIONREAD, &bytes_available);
    if (status < 0)
        return status;
    return bytes_available;
}

inline bool is_socket_alive(int sock)
{
    int error_code;
    socklen_t error_code_size = sizeof(error_code);
    int status = getsockopt(sock, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);
    if (status < 0)
    {
        err_msg("getsockopt SO_ERROR returned ", status, "; error code ", errno);
        return false;
    }

    return error_code == 0;
}
