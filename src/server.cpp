#include <iostream>
#include <signal.h>
#include <thread>
#include <atomic>
#include "tcpsrv.h"
#include "traces.h"


using namespace std;

void on_connection_established(shared_ptr<Connection>& c)
{
    LoopRate loop_rate(1e-3);
    char buf[1024];

    while (true)
    {
        int len = c->read(buf, sizeof(buf), false);
        if (len == 0)
        {
            dbg_msg("no messages");
        }
        else if (len < 0)
        {
            dbg_msg("connection closed");
            break;
        }
        else
        {
            string msg(buf, len);
            dbg_msg("new message at ", to_string(get_time_usec()), ": ", msg);
        }

        string msg = to_string(get_time_usec());
        c->write(msg);

        loop_rate.wait();
    }
}

void run()
{
    info_msg("server initializing");
    TCPSrv server(11015);

    auto f = [&server]() { server.stop(); };
    auto f2 = make_shared<signal_handler_t>(f);
    set_sigint_handler(f2);

    info_msg("waiting for incoming connection..");

    while (true)
    {
        auto c = server.wait_for_connection();

        if (c)
        {
            info_msg("connection established");
            on_connection_established(c);
        }
        else
        {
            info_msg("server closed");
            break;
        }
    }
}

int main()
{
    try
    {
        run();
    }
    catch (exception& e)
    {
        err_msg("uncaught exception", e.what());
        return -1;
    }

    return 0;
}

