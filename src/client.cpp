#include <iostream>
#include <signal.h>
#include "tcpclient.h"
#include "traces.h"
#include "common.h"

using namespace std;

void run()
{
    info_msg("client initializing");
    bool stopped = false;

    auto f = [&stopped]() { stopped = true; };
    auto f2 = make_shared<signal_handler_t>(f);
    set_sigint_handler(f2);

    LoopRate loop_rate(1e-3);
    TCPClient client("127.0.0.1", 11015);

    while (!stopped)
    {
        auto ans = client.read();
        if (get<0>(ans) < 0)
        {
            info_msg("connection closed; read returned ", get<0>(ans));
            break;
        }
        else if (get<0>(ans) == 0)
        {
            info_msg("no new messages");
            string msg = "sending back the message: empty";
            client.write(msg);
        }
        else
        {
            info_msg("new message received: ", get<1>(ans));
            string msg = "sending back the message: " + get<1>(ans);
            client.write(msg);
        }

        loop_rate.wait();
    }
}

int main()
{
    try
    {
        run();
    }
    catch (exception& e)
    {
        err_msg("uncaught exception: ", e.what());
        return -1;
    }

    return 0;
}
