#ifndef __tcp_clinet_h__
#define __tcp_clinet_h__

#include <string>
#include <tuple>

class TCPClient
{
private:
	int sock;

public:
	TCPClient(std::string const& host, int port);
	~TCPClient();

	bool write(char const* s, int len);
	bool write(std::string const& s);
	int read(char* s, int len, bool blocking=true);
    std::tuple<int, std::string> read(bool blocking=true);
    bool available();
	int size();
};

#endif // __tcp_clinet_h__
