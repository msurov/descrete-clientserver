#pragma once
#include <memory>
#include <string>
#include <tuple>


class TCPSrv;
class Connection;


/*
 * Connection
 */
 
class Connection
{
private:
	friend class TCPSrv;
	int remote_sock;

	Connection(int remotre_sock);
	Connection(Connection const&);

public:
	~Connection();
	int write(char const* s, int len);
	int write(std::string const& s);
	int read(char* s, int len, bool blocking=true);
	int available();
	std::tuple<int, std::string> read(bool blocking=true);
	bool alive();
};


/*
 * tcp server
 */

class TCPSrv
{
private:
	int 	port;
	int 	srv_sock;

private:
	void init_server();
	void stop_server();

public:
	TCPSrv(int port);
	~TCPSrv();

	std::shared_ptr<Connection> wait_for_connection();
	void run();
	void stop();
};
